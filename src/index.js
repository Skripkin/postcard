document.addEventListener('DOMContentLoaded', () => {
  const card = document.querySelector('.card')

  card.addEventListener('click', function () {
    this.classList.toggle('is-opened')
  }, true)
})

const spinner = document.querySelector('.spinner')
window.addEventListener('load', () => {
  spinner.classList.add('spinner--hidden')
})
